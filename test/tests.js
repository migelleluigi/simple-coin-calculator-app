QUnit.test( "Should accepts", function( assert ) {
	assert.deepEqual( CoinCalculator.stringToPence("4"), 4, "single digit" );
	assert.deepEqual( CoinCalculator.stringToPence("85"), 85, "double digit" );
	assert.deepEqual( CoinCalculator.stringToPence("197p"), 197, "pence symbol" );
	assert.deepEqual( CoinCalculator.stringToPence("2p"), 2, "pence symbol single digit" );
	assert.deepEqual( CoinCalculator.stringToPence("1.87"), 187, "pounds decimal" );
	assert.deepEqual( CoinCalculator.stringToPence("£1.23"), 123, "pounds symbol decimal" );
	assert.deepEqual( CoinCalculator.stringToPence("£2"), 200, "single digit pounds symbol" );
	assert.deepEqual( CoinCalculator.stringToPence("£10"), 1000, "double digit pounds symbol" );
	assert.deepEqual( CoinCalculator.stringToPence("£1.87p"), 187, "pound and pence symbol decimal" );
	assert.deepEqual( CoinCalculator.stringToPence("£1p"), 100, "missing pence" );
	assert.deepEqual( CoinCalculator.stringToPence("£1.p"), 100, "missing pence decimal point present" );
	assert.deepEqual( CoinCalculator.stringToPence("001.41p"), 141, "buffered zeroes" );
	assert.deepEqual( CoinCalculator.stringToPence("4.235p"), 424, "rounding with pence symbol" );
	assert.deepEqual( CoinCalculator.stringToPence("1.257422457p"), 126, "rounding with pounce and pence symbols" );
});

QUnit.test( "Should not accept", function( assert) {
	assert.deepEqual( CoinCalculator.stringToPence(""), 0, "empty string" );
	assert.deepEqual( CoinCalculator.stringToPence("1x"), 0, "non-numeric, non-symbol character" );
	assert.deepEqual( CoinCalculator.stringToPence("£1x.0p"), 0, "non-numeric, non-symbol character along with valid symbols" );
	assert.deepEqual( CoinCalculator.stringToPence("£p"), 0, "missing digits" );
});