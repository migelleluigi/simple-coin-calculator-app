/**
 * Coin Calculator
 * Module for calculating the given number of pennies to the minimum number of Sterling coins needed to make that amount
 */
var CoinCalculator = (function(){
    "use strict";

    // Static Values
    var config = {
        coins : [200, 100, 50, 20, 10, 5, 2, 1]
    };

    // Dynamic Values
    var state = {};

    var bindEvents = function() {
        var calculatorForm   = state.container.find('.js-calculator');
        var outputCointainer = state.container.find('.js-output');
        var inputAmount      = state.container.find('.js-amount');

        calculatorForm.off('submit');
        calculatorForm.on('submit', function(e){
            e.preventDefault();
            var amount = calculate(inputAmount.val());
            var output = Templates.renderResult(amount);
            outputCointainer.html(output);
        });
    };


    /**
     * Calculate given amount into minimum coins needed
     * @param {String}
     * @return {Array} returns array of objects
     */
    var calculate = function(str) {
        var amount      = stringToPence(str);
        var coins       = config.coins;
        var coinsLength = coins.length;
        var minCoins    = [];

        for(var i = 0; i<coinsLength; i++) {
            if(coins[i] <= amount){
                var coinUsed = coins[i];
                var coinPieces = Math.floor(amount/coins[i]);
                
                minCoins.push({
                    coin: coinUsed, 
                    coinMultiple: coinPieces
                });

                amount = amount % coinUsed;
            }
        };

        return minCoins;
    };


    /**
     * Convert given string into pence
     * @param {String} str - the string to be converted
     * @return {Number} convertedNumber - returns an integer converted to pence
     */
    var stringToPence = function(str) {
        if(!Util.isValidPence(str)) {
            return 0;
        }
        var convertedNumber = str.replace('£', '');
        var hasPoundSymbol  = str.indexOf('£') !== -1;
        convertedNumber = parseFloat(convertedNumber);

        if(Util.hasDecimal(convertedNumber) || hasPoundSymbol) {
            convertedNumber = convertedNumber * 100;
        }

        convertedNumber = Math.round(convertedNumber);
        return convertedNumber;
    };

    var init = function(container) {
        state.container = $(container);
        bindEvents();
    };

    return {
        init : init,
        stringToPence : stringToPence
    }
})();