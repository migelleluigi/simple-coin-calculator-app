/**
 * Utility for Coin Calculator
 * Module for available methods that can be used in several pages
 */
var Util = (function(){
    "use strict";

    var showSterlingCoins = function(num) {
        var coinView = num + 'p';
        if(num >= 100) {
            coinView = '£'+num/100;
        }
        return coinView;
    };

    /**
     * Check if number has decimal
     * @param {Number} num - the number to be checked
     * @return {Boolean} - returns true if has valid decimal
     */
    var hasDecimal = function(num) {
        return num % 1 !== 0; 
    };

    /**
     * Check the value if valid
     * @param {String} str - the value to be checked
     * @return {Boolean} - returns true if allowed based on given rules
     * Input should not be empty
     * Input should be numeric
     * Input should contain valid symbol
     * Input should have atleast one digit
     */
    var isValidPence = function(str) {
        return str.match(/^(?=.*[0-9])([£p.\d])+$/) ? true : false;
    };

    return {
        hasDecimal : hasDecimal,
        isValidPence : isValidPence,
        showSterlingCoins : showSterlingCoins
    }
})();