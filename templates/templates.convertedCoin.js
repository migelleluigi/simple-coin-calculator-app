var Templates = Templates ? Templates : {};

Templates.renderResult = function(amount) {
	var template = "";
	amount.forEach(function(val){
		template += "<tr>";
        template += "<td>" +val.coinMultiple +"</td>";
        template += "<td>x</td>";
        template += "<td class='mdl-data-table__cell--non-numeric'>"+ Util.showSterlingCoins(val.coin) +"</td>";
        template += "</tr>";
	});
	return template;
};